package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    double getCelcius(String isbn){
        Double val = Double.parseDouble(isbn);
        val = (val*9/5) + 32;
        return val;
    }
}
